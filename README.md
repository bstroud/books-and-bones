# Brandi Stroud
* EID: bms3457
* GitLab ID: bstroud
- Phase 1:
    * Estimated completion time for each member (hours: int): 25
    * Actual completion time for each member (hours: int): 18
- Phase 2:
    * Estimated completion time for each member (hours: int): 30
    * Actual completion time for each member (hours: int): 30 
- Phase 3:
    * Estimated completion time for each member (hours: int): 15
    * Actual completion time for each member (hours: int): 15
- Phase 4:
    * Estimated completion time for each member (hours: int): 10
    * Actual completion time for each member (hours: int): 6

# Sahil Mittal
* EID: sm64274 
* GitLab ID: sahilmittal917 
- Phase 1: 
    * Estimated completion time for each member (hours: int): 25 
    * Actual completion time for each member (hours: int): 15 
- Phase 2:
    * Estimated completion time for each member (hours: int): 30 
    * Actual completion time for each member (hours: int): 30 
- Phase 3:
    * Estimated completion time for each member (hours: int): 15 
    * Actual completion time for each member (hours: int): 15  
- Phase 4: 
    * Estimated completion time for each member (hours: int): 10
    * Actual completion time for each member (hours: int):6

# Laura Shub 
* EID: lrs2553 
* GitLab ID: laurashub 
- Phase 1: 
    * Estimated completion time for each member (hours: int): 25 
    * Actual completion time for each member (hours: int): 15 
- Phase 2: 
    * Estimated completion time for each member (hours: int): 30 
    * Actual completion time for each member (hours: int): 35 
- Phase 3: 
    * Estimated completion time for each member (hours: int): 15 
    * Actual completion time for each member (hours: int): 15 
- Phase 4: 
    * Estimated completion time for each member (hours: int): 10 
    * Actual completion time for each member (hours: int): 6


# Christopher Jones 
* EID: caj2662 
* GitLab ID: tartarut 
- Phase 1: 
    * Estimated completion time for each member (hours: int): 25  
    * Actual completion time for each member (hours: int): 7 
-  Phase 2: 
    * Estimated completion time for each member (hours: int): 30 
    * Actual completion time for each member (hours: int): DROPPED THE CLASS 

# Gilberto Martinez 
* EID: gam2894
* GitLab ID: notgil
- Phase 1: 
    * Estimated completion time for each member (hours: int): 25 
    * Actual completion time for each member (hours: int): 8 
- Phase 2: 
    * Estimated completion time for each member (hours: int): 30 
    * Actual completion time for each member (hours: int): 40 
- Phase 3: 
    * Estimated completion time for each member (hours: int): 15 
    * Actual completion time for each member (hours: int): 15 
- Phase 4: 
    * Estimated completion time for each member (hours: int): 10 
    * Actual completion time for each member (hours: int): 8

#Phase 1: 
    Git SHA:c9617688e5d68ada39eba7579947e6895e911a9d 
    link to GitLab pipelines: N/A 
    link to website: https://www.booksandbones.me 
    comments: 
        -some of the Google Maps js can be attributed to Google's tutorial on the Maps API  

#Phase 2:  
    Git SHA: b92ad61dd8bbdbbde72fa54801ce023cb9fd6eab  
    link to GitLab pipelines: https://gitlab.com/bstroud/books-and-bones/pipelines  
    link to website: https://www.booksandbones.me  
    comments:
        -some of the Google Maps js can be attributed to Google's tutorial on the Maps API  
        -instafeed.js came from this Github: https://github.com/stevenschobert/instafeed.js/blob/master/instafeed.js  
        -our main.py is backend/manage.py  
        -our index.js is books_and_bones/src/index.js   
        
#Phase 3: 
    Git SHA: 7a01b81d7dea1e72b2498ded1ab7de69a9d95488  
    link to GitLab pipelines: https://gitlab.com/bstroud/books-and-bones/pipelines  
    link to website: https://www.booksandbones.me 
    comments:
        -some of the Google Maps js can be attributed to Google's tutorial on the Maps API 
        -instafeed.js came from this Github: https://github.com/stevenschobert/instafeed.js/blob/master/instafeed.js 
        -our main.py is backend/manage.py 
        -our index.js is books_and_bones/src/index.jsv  

#Phase 4:
    Git SHA: 3e2efb1d5a389a1299610ceb0ea1a5b3f49b2693
    link to GitLab pipelines: https://gitlab.com/bstroud/books-and-bones/pipelines
    link to website: https://www.booksandbones.me
