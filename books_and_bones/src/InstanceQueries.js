const url = "http://api.booksandbones.me/" 

export async function getMuseumPage(offset) {
    var state={'data': []}
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
        targetUrl = 'http://api.booksandbones.me/museums/?format=json&limit=12&offset='
    fetch(proxyUrl + targetUrl + offset)
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
    return state.data;
}

export async function getEventPage(offset) {
    var state={'data': []}
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
        targetUrl = 'http://api.booksandbones.me/events/?format=json&limit=12&offset='
    fetch(proxyUrl + targetUrl + offset)
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
    return state.data;}

export async function getLibraryPage(offset) {
	var state={'data': []}
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
        targetUrl = 'http://api.booksandbones.me/libraries/?format=json&limit=12&offset='
    fetch(proxyUrl + targetUrl + offset)
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
    return state.data;
}

export async function getLibraryData(id) {
    var state={'data': []}
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
        targetUrl = 'http://api.booksandbones.me/libraries/'
    fetch(proxyUrl + targetUrl + id)
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
    return state.data;
}

export async function getMuseumData(id) {
    var state={'data': []}
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
        targetUrl = 'http://api.booksandbones.me/museums/'
    fetch(proxyUrl + targetUrl + id)
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
    return state.data;
}

export async function getEventData(id) {
    var state={'data': []}
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
        targetUrl = 'http://api.booksandbones.me/events/'
    fetch(proxyUrl + targetUrl + id)
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
    return state.data;
}
