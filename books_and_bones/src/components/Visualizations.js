import React from "react";
import ReactDOM from 'react-dom';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import { BarChart, PieChart } from "react-d3-components";
import * as d3 from 'd3';
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
} from "react-simple-maps";

class Visualizations extends React.Component{
	constructor(props) {
		super(props)
        this.state = { }

        this.displayLibraryDistribution = this.displayLibraryDistribution.bind(this);
        this.displayMuseumTypeVisualization = this.displayMuseumTypeVisualization.bind(this);
        this.displayEventVisualization = this.displayEventVisualization.bind(this);
        this.displayDeveloperVisualization = this.displayDeveloperVisualization.bind(this);
    }

    displayLibraryDistribution(){
    	this.setState({show: "LibraryDistributions"})
    }

    displayMuseumTypeVisualization(){
    	this.setState({show: "MuseumTypes"})
    }

    displayEventVisualization(){
    	this.setState({show: "EventVis"})
    }

    displayDeveloperVisualization(){
		this.setState({show: "DeveloperVis"})
	}

    render(){ return (
    <div className="container">
     <h1 className="my-4">Data Visualizations</h1>
     	<div className="row">
     	    <button onClick={this.displayLibraryDistribution} className="model-button" type="button">Library Distributions</button>
     	    <p>&emsp;</p>
            <button onClick={this.displayMuseumTypeVisualization} className="model-button" type="button">Museum Types</button>
            <p>&emsp;</p>
            <button onClick={this.displayEventVisualization} className="model-button" type="button">Events</button>
            <p>&emsp;</p>
            <button onClick={this.displayDeveloperVisualization} className="model-button" type="button">Developer Visualizations</button>
            <p>&emsp;</p>
                <br></br>
                <br></br>
        </div>
        <SwitchVisualization show={this.state.show}/>
    </div>

                
    )}
}

function SwitchVisualization(props){
	switch(props.show){
		case "LibraryDistributions":
			return(<LibraryDistribution/>)
			break;
		case "MuseumTypes":
			return(<MuseumTypes/>)
			break;
		case "EventVis":
			return(<EventVis/>)
			break;
		case "DeveloperVis":
			return(<DeveloperVis/>)
			break;
		default:
			return(<div>Click above to see our data visualizations!</div>)
	}
	
}

class LibraryDistribution extends React.Component {

	constructor(props){
		super(props);
		this.state = { label: "Name", values : [] }
	}

	componentDidMount(){
		var data = {
			label: 'somethingA',
			values: [{x: 'SomethingA', y: 10}, {x: 'SomethingB', y: 4}, {x: 'SomethingC', y: 3}]
		};
		fetch('https://api.booksandbones.me/libraries?format=json&limit=30')
		.then(response => response.json())
		.then(response => this.parseLibraries(response.results))
		.then(arr => this.updateValues(arr))
	}

	parseLibraries(results){
		var arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		for (var i=0;i<results.length;i++){
			var lib = results[i]
			var name = lib.name
			var district = parseInt(lib.district,10)
			arr[district-1]++;
		}
		return arr;
	}

	updateValues(arr){
		var values = []
		for(var i=0;i<arr.length;i++){
			values.push({x:"District "+(i+1)+" : "+Math.round((arr[i])/23*100)+"%", y:arr[i]})
		}
		this.setState({values:values})
	}

	tooltipPie = function(x, y) {
        return x + ": " + y.toString();
        };
	render(){
		return (
			<div>
				<p>Library Distributions</p>
				<PieChart
					data={this.state}
					width={600}
					height={400}
					margin={{top: 10, bottom: 10, left: 100, right: 100}}
					sort={null}
					tooltipHtml={this.tooltipPie}
					tooltipMode={'element'}/>
			</div>
		)
	}
}

const weekdays = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"]
class EventVis extends React.Component{

	constructor(props) {
		super(props)
        this.state = { "counts" : [0, 0, 0, 0, 0, 0, 0] }
        this.collectEvents = this.collectEvents.bind(this);
        this.getEventCount = this.getEventCount.bind(this);
    }

   	componentDidMount(){
		this.getEventCount(0)
	}

	getEventCount(offset){
		fetch('https://api.booksandbones.me/events/?&format=json')
            .then(response => response.json())
            .then(response => this.collectEvents(0, response['count']))
	}

	collectEvents(offset, count){
		fetch('https://api.booksandbones.me/events/?&format=json&offset='+offset+'&limit=9')
            .then(response => response.json())
            .then(response => {response['results'].map(function(item, index){
                this.determineWeekday(item)}, this)})
        if(offset+9 <= count){
        	this.collectEvents(offset+9, count)
        }
	}
	
	determineWeekday(event){
		var dt = new Date(event['date'])
		var weekday = dt.getDay()

		var temp = this.state.counts
		temp[weekday]++
		this.setState({"counts":temp})
	}

    render(){
    	var data=[ {label: "Name", values : [] }]
    	var i = 0;
    	for(var i = 0; i < 7; i++){
    		data[0].values.push({"x": weekdays[i], "y": this.state.counts[i]})
    	}
		return ( 
			<div>
		  	<p>Events</p>
			<BarChart
			data={data}
			width={1000} 
			height={400}
      		margin={{ top: 10, bottom: 50, left: 50, right: 10 }}
			xAxis={{tickDirection:'diagonal'}}    
		/>
		</div>)
	}
}

class MuseumTypes extends React.Component {
	constructor(props) {
		super(props)
        this.state = { label: "Name", values : [] }
        this.getMuseumTypes = this.getMuseumTypes.bind(this);
        this.getTypeCount = this.getTypeCount.bind(this);
    }
	

	componentDidMount(){
		this.getMuseumTypes()
	}

	getMuseumTypes(){
		fetch('https://api.booksandbones.me/museum-distinct?field=mus_type&format=json')
            .then(response => response.json())
            .then(response => {response.map(function(item){
				this.getTypeCount(item)}, this)})
	}

	getTypeCount(type){
		fetch('https://api.booksandbones.me/museums/?mus_type='+type+'&format=json')
            .then(response => response.json())
            .then( response =>
            	this.setState({ values : this.state.values.concat({"label": type, "value": response['count']}) }));
	}
	bubbleClick = (label) =>{
		
	}
	
	render(){
		  return(
		  <div>
		  <p>Museum Types</p>
			  <div margin={100}>
		  <BubbleChart
			graph= {{
			  zoom: 1,
			  offsetX: -0.05,
			  offsetY: -0.01,
			}}
			width={1000}
			height={1000}
			showLegend={true} // optional value, pass false to disable the legend.
			legendPercentage={10} // number that represent the % of with that legend going to use.
			legendFont={{
				  family: 'Arial',
				  size: 12,
				  color: '#000',
				  weight: 'bold',
				}}
			valueFont={{
				  family: 'Arial',
				  size: 12,
				  color: '#fff',
				  weight: 'bold',
				}}
			labelFont={{
				  family: 'Arial',
				  size: 16,
				  color: '#fff',
				  weight: 'bold',
				}}
			//Custom bubble/legend click functions such as searching using the label, redirecting to other page
			bubbleClickFunc={this.bubbleClick}
			// legendClickFun={this.legendClick}
			data={this.state.values}
		  />
		  </div>
		  </div>);
	}

}

function DeveloperVis(props){
	return(
		<div>
			<MissionMap/>
			<EmployeeBubble/>
			<DestinationPie/>
		</div>
		)
}


const wrapperStyles = {
  width: "100%",
  maxWidth: 980,
  margin: "0 auto",
}


const cnc = {"United States of America":"United States",
	 "South Korea": "Korea, Republic of", 
	 "Russia":"Russian Federation"}
const opacities = {22:"1", 3:"0.7", 2:"0.5", 1:"0.3"}

class MissionMap extends React.Component {
	constructor(props) {
		super(props)
        this.state = { }
       	this.updateMissionCount = this.updateMissionCount.bind(this);
        this.getMissions = this.getMissions.bind(this);
    }

    componentDidMount(){
		fetch('https://api.budgetfor.space/missions?results-per-page=60&format=json')
		.then(response => response.json())
		.then(response => {
				response.missions.map(function(item){
				this.updateMissionCount(item)}, this)})
	}

	updateMissionCount(item){
		var country = item['country']
		var count = this.state[country]
		if (count === undefined){
			this.setState({[country] : 1})
		} else {
			var newCount = count+1
			this.setState({[country] : newCount})
		}
	}

    getMissions(name){
    	if (name in cnc){ name = cnc[name]}
    	if(this.state[name] == undefined){
    		return("#CFD8DC")
    	} else {
    		var op = opacities[this.state[name]]
    		var full_col = "rgb(0, 128, 128, " + op+")"
    		return (full_col)
    	}
    }

 render() {
 	if (this.state["United States"] == 22){
    return (
      <div style={wrapperStyles}>
      	<p>Mission Heatmap by Country</p>
        <ComposableMap
          projectionConfig={{
            scale: 205,
            rotation: [-11,0,0],
          }}
          width={980}
          height={551}
          style={{
            width: "100%",
            height: "auto",
          }}
          >
          <ZoomableGroup center={[0,20]} disablePanning>
            <Geographies geography="https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-50m.json">
              {(geographies, projection) => geographies.map((geography, i) => geography.id !== "ATA" && (
                <Geography
                  key={i}
                  geography={geography}
                  projection={projection}
                  style={{
                    default: {
                      fill: this.getMissions(geography.properties.NAME),
                      stroke: "#607D8B",
                      strokeWidth: 0.75,
                      outline: "none",
                    },
                    hover: {
                      fill: "#004c4c",
                      stroke: "#607D8B",
                      strokeWidth: 0.75,
                      outline: "none",
                    },
                  }}
                />
              ))}
            </Geographies>
          </ZoomableGroup>
        </ComposableMap>
      </div>
    )}
    return(<div>Loading...</div>)
  }
}


class EmployeeBubble extends React.Component {
	constructor(props) {
		super(props)
        this.state = { label: "Name", values : [] }
        this.getMissionCount = this.getMissionCount.bind(this);

    }

    componentDidMount(){
		fetch('https://api.budgetfor.space/organizations?results-per-page=54&format=json')
            .then(response => response.json())
    		.then(response => {{response['organizations'].map(function(item){this.getMissionCount(item)}, this)}})
	}

	getMissionCount(item){
		if (!(item.employees == undefined)){
			this.setState({ values : this.state.values.concat({"label": item.name, "value": item.employees}) });
		}
	}

	render(){
		return(
		  <div>
		  <p>Employees per Organization</p>
			  <div margin={100}>
		  <BubbleChart
			graph= {{
			  zoom: .9,
			  offsetX: -0.05,
			  offsetY: -0.01,
			}}
			width={1000}
			height={1000}
			showLegend={true} // optional value, pass false to disable the legend.
			legendPercentage={20} // number that represent the % of with that legend going to use.
			legendFont={{
				  family: 'Arial',
				  size: 12,
				  color: '#000',
				  weight: 'bold',
				}}
			valueFont={{
				  family: 'Arial',
				  size: 12,
				  color: '#fff',
				  weight: 'bold',
				}}
			labelFont={{
				  family: 'Arial',
				  size: 16,
				  color: '#fff',
				  weight: 'bold',
				}}
			//Custom bubble/legend click functions such as searching using the label, redirecting to other page
			bubbleClickFunc={this.bubbleClick}
			// legendClickFun={this.legendClick}
			data={this.state.values}
		  />
		  </div>
		  </div>);
	}

}


const graphName = {"orbit":"Orbit", "asteroid":"Asteroid", "meteorite": "Meteorite",
	 "planet":"Planet", "satellite":"Satellite"}
class DestinationPie extends React.Component {
	constructor(props) {
		super(props)
        this.state = { }
        this.getTypeCount = this.getTypeCount.bind(this);

    }

    componentDidMount(){
		fetch('https://api.budgetfor.space/destinations?results-per-page=90&format=json')
            .then(response => response.json())
    		.then(response => this.getTypeCount(response['destinations']))
			.then(arr => this.fixState(arr))
	}

	getTypeCount(itemList){
		var typeCounts = {};
		itemList.map(function(item){
			var type = item.type
			if (typeCounts[type] == undefined){
				typeCounts[type] = 1
			} else {
				var cur = typeCounts[type]
				var newVal = cur+1
				typeCounts[type] = newVal
			}	
		})
		return typeCounts
	}

	fixState(arr){
		var vals = []
		for (var type in arr){
			var temp = {x: this.fixName(type, arr[type]), y: arr[type]}
			vals.push(temp)
		}
		this.setState({ label: "Name", values : vals})
	}

	fixName(x, y){
		return( graphName[x] + ": " + y )
	}

    tooltipPie = function(x, y) {
        return x + ": " + y.toString();
    };
	
	render(){
		if (this.state.label == undefined){
			return(<div>Loading...</div>)
		}
		return (
			<div>
				<p>Destination Types</p>
				<PieChart
					data={this.state}
					width={600}
					height={400}
					margin={{top: 10, bottom: 10, left: 100, right: 100}}
					sort={null}
					tooltipHtml={this.tooltipPie}
					tooltipMode={'element'}/>
			</div>
		)
	}

}
	

export default Visualizations;
