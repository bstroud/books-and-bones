import React from "react";
import { Link } from "react-router-dom";
import Pagination from "./Pagination.js";
import { Button, ButtonToolbar, DropdownButton, Dropdown } from "react-bootstrap";
import Search from "./Search.js";
import ReactDOM from "react-dom";
import Highlighter from "react-highlight-words";


class Events extends React.Component{
    constructor(props) {
        super(props)
        this.state = { instancesPerPage: 9,
        data: [], 
        currentPage: null, 
        totalPages: null, 
        refresh: true, 
        activeQueries: {},
        activeSearch: ''}
        this.updateFilter = this.updateFilter.bind(this);
        this.updateSearch = this.updateSearch.bind(this);
        this.getFilterString = this.getFilterString.bind(this);
        this.reset = this.reset.bind(this);
    }

    onPageChanged = data => {
        this.setState({'refresh':false})
        const { currentPage, totalPages, pageLimit } = data;

        var offset=(currentPage-1)*pageLimit
        var targetUrl = 'https://api.booksandbones.me/events/?format=json'

        fetch(targetUrl+"&offset="+offset+"&search="+this.state.activeSearch+"&limit="+pageLimit+this.getFilterString())
            .then(response => response.json())
            .then(response => {
                const data=response['results']
                this.setState({ currentPage, data, totalPages });
            })
    
        };

    componentDidMount() {
        this.reset()
    }

    updateFilter(category, entry){
        var actives = {...this.state.activeQueries}
        //update active queries
        switch (filtering[category]){
            case "place_name":
                actives.place_name = entry;
                break;
            case "place__district":
                actives.place__district = entry;
                break;
        }
        if (category === "day") {
                var day = ""
                switch (entry) {
                case "Monday":
                    day = "mon";
                    break;
                case "Tuesday":
                    day = "tue";
                    break;
                case "Wednesday":
                    day = "wed";
                    break;
                case "Thursday":
                    day = "thu";
                    break;
                case "Friday":
                    day = "fri";
                    break;
                case "Saturday":
                    day = "sat";
                    break;
                case "Sunday":
                    day = "sun";
                    break;
            }
            actives.day = day;
            this.setState({'activeQueries':actives}, () => {
            var targetUrl = 'https://api.booksandbones.me/events/?format=json'
            fetch(targetUrl+this.getFilterString()+"&day="+day)
                .then(response => response.json())
                .then(response => {
                    this.setState({'total':response['count'], 'refresh': true, 'activeQueries':actives })})
            });
        }else if(category === "Sort by Name"||category==="Sort by Date"){
            var sort = ""
            switch (entry) {
            case "Ascending":
                sort = (category==="Sort by Name"?"name":"date");
                break;
            case "Descending":
                sort = "-"+(category==="Sort by Name"?"name":"date");
                break;
            }
            actives.sort = sort;
            this.setState({'activeQueries':actives}, () => {
            var home = 'https://api.booksandbones.me/events/?format=json&';
            var targetUrl =home+this.getFilterString()+"&search="+this.state.activeSearch 
            fetch(targetUrl)
                .then(response => response.json())
                .then(response => {
                    this.setState({'total':response['count'], 'refresh': true, 'activeQueries':actives })})
            });
        }else {
            this.setState({'activeQueries':actives}, () => {
            var targetUrl = 'https://api.booksandbones.me/events/?format=json'
            fetch(targetUrl+this.getFilterString()+"&search="+this.state.activeSearch)
                .then(response => response.json())
                .then(response => {
                    this.setState({'total':response['count'], 'refresh': true, 'activeQueries':actives })})
            });
        }
    }

    getFilterString(){
        var curFilters = ""
        for (var category in this.state.activeQueries) {
            curFilters += "&" + category + "=" + this.state.activeQueries[category]
        }
        return curFilters
    }

    updateSearch(data){
        var targetUrl = 'https://api.booksandbones.me/events/?format=json'
        fetch(targetUrl+"&search="+data+this.getFilterString())
            .then(response => response.json())
            .then(response => {this.setState({'total':response['count'], 'activeSearch':data, 'refresh':true})
            })

    }

    reset(){
        this.setState({'data':[]})
        var targetUrl = 'https://api.booksandbones.me/events/?format=json'
        fetch(targetUrl)
            .then(response => response.json())
            .then(response => {
                this.setState({'total':response['count'], 'refresh': true, 'activeQueries': {}, 'activeSearch':"" })})
    }

    render(){
        if(this.state.total){
            if (this.state.activeSearch !== "") {
                var printSearch = "Search: " + this.state.activeSearch;
            } else {
                var printSearch = "";
            }
            return(
                <div>
                <div className="container">
                <h1 className="my-4">Events</h1>
                <p>{printSearch}</p>
                <div className="row">
                <EventFilters func={this.updateFilter} activeQueries={this.state.activeQueries}/>
                <p>&emsp;</p>
                <Button className="reset" onClick={this.reset}>Reset</Button>
                <p>&emsp;</p>
                <Search updateSearch={this.updateSearch} id="modelsearchbar"/>
                </div>
                <br></br>
                </div>
                <div className="container">
                <div className="row">
                <div className="card-group">
                {this.state.data.map(function(item, index){
                    var districtDict = [4,1,7,3,4,2,10,1,9,1,3,2,10,3,8,3,7,5,7,6,9,7,9]

                return (
                    <EventCard
                        name={item.name}
                        website={item.permalink}
                        description={item.description}
                        id={item.id}
                        image={item.event_image_url}
                        contact_info={item.contact_info}
                        time={item.pub_date}
                        location = {item.place_name}
                        district = {districtDict[item.place-1]}
                        activeSearch={this.state.activeSearch}
                    />)
                    }, this)
                }
                </div>
                </div>
                </div>
                <div className="container">
                    <div className="row">
                         <Pagination 
                            totalRecords={this.state.total} 
                            pageLimit={this.state.instancesPerPage} 
                            pageNeighbours={1} 
                            onPageChanged={this.onPageChanged}
                            refresh={this.state.refresh}/>
                    </div>
                    </div>
                    </div>);
                } else {
                 return(
                        <div>
                        <div className="container">
                         <h1 className="my-4">Events</h1>
                         <EventFilters func={this.updateFilter} activeQueries={this.state.activeQueries}/>
                         <p>Search: {this.state.activeSearch}</p>
                         <p>No results found</p>
                         <Button className="reset" onClick={this.reset}>Reset</Button>
                         </div>
                         <br></br>
                         </div>
                         )
                }
    }
    
}

class EventFilters extends React.Component {
    constructor(props) {
        super(props)
        this.setState({ activeQueries: props.activeQueries})
        this.state=props.state
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ activeQueries: nextProps.activeQueries }); 
    }

    checkTitle(variant){
        for (var category in this.state.activeQueries) {
            if (category === filtering[filtering[variant]]){
                return this.state.activeQueries[category]
            }
        }
        return variant
    }

    render(){
        if (this.state){
            return (
                <div>
                    <ButtonToolbar>
                    {['Day', 'Host Institution', 'Host District', 'Sort by Name', 'Sort by Date'].map(
                        variant => (
                            <DropdownButton
                            className="dropdown-button"
                            title={this.checkTitle(variant)}
                            variant={variant.toLowerCase()}
                            id={'dropdown-variants-${variant}'}
                            key={variant}
                            >
                            <DropdownMenu category={variant} func={this.props.func} active={this.state.activeQueries}/>

                </DropdownButton> 
                ),
            )}
            </ButtonToolbar>
            </div>);
        }
        return(<div></div>)
    }
}

class DropdownMenu extends React.Component{
    constructor(props){
        super(props)
        this.state={}
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ activeQueries: nextProps.active }, () => this.renderPosts())
    }

    componentDidMount() {
        this.setState({ activeQueries: this.props.active }, () => this.renderPosts())

    }

    getFilterString(){
        if(this.state){
        var curFilters = ""
        for (var category in this.state.activeQueries) {
            curFilters += "&" + category + "=" + this.state.activeQueries[category]
        }
        return curFilters
    } return ""
    }

    renderPosts = async() => {
        if (this.props.category==="Day"){
            this.setState({'entries':["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]})
        } else if (this.props.category==="Sort by Name" || this.props.category==="Sort by Date") {
            this.setState({'entries':["Ascending","Descending"]})

        } else {
            var targetUrl = 'https://api.booksandbones.me/event-distinct?field='+filtering[this.props.category]+this.getFilterString()+'&format=json'
            fetch(targetUrl)
            .then(response => response.json())
            .then(response => this.setState({'entries':response }));
        }
    }

    render(){
        if (this.state && this.state.entries){
            return(
                <div> 
                {this.state.entries.map(function (entry) {
                    return ( 
                       <Dropdown.Item onClick={() => this.props.func(filtering[this.props.category], entry)}>{entry}</Dropdown.Item> )
                    }, this)
                }
                </div>
            )
        }
        return (<div></div>)
       }
}

function EventCard(props){
    if (props.activeSearch === "") {
        return(
            <div className="col-lg-4 col-sm-6 portfolio-item">
                <div className="card h-100">
                    <img className="card-img-top" src={props.image} alt=""/>
                    <div className="card-body">
                    <h4 className="card-title">
                        <Link to={'/event/'+props.id}>{props.name}</Link>
                    </h4>
                        <div className="card-body">
                            <p className="card-text">{props.time}</p>
                            <p className="card-text">{props.description.substr(0, 100)+"..."}</p>
                            <p className="card-text">Hosted at: {props.location}</p>
                            <p className="card-text">District: {props.district}</p>
                            <p className="card-text"><a href={props.website}>Website</a></p>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        if (props.description.toLowerCase().includes(props.activeSearch.toLowerCase())) {
            var index = props.description.toLowerCase().indexOf(props.activeSearch.toLowerCase());
            var len = props.activeSearch.length
            if(index < 50){
                var beg = ""
                var new_index = 0;
            } else {
                var beg = "..."
                var new_index = index - 50;
            }
            var description = beg + props.description.substr(new_index, len + 100)+"...";
        } else {
            var description = props.description.substr(0, 100)+"...";
        }
        return (
            <div className="col-lg-10 col-sm-10">
                <div className="card h-100">
                    <div className="card-body">
                        <Link to={'/event/'+props.id}><Highlighter
                        highlightClassName="YourHighlightClass"
                        searchWords={props.activeSearch.split(" ")}
                        autoEscape={true}
                        textToHighlight={props.name}/></Link>

                        <p className="card-text"><Highlighter
                        highlightClassName="YourHighlightClass"
                        searchWords={props.activeSearch.split(" ")}
                        autoEscape={true}
                        textToHighlight={description}/></p>

                        <p className="card-text"><Highlighter
                        highlightClassName="YourHighlightClass"
                        searchWords={props.activeSearch.split(" ")}
                        autoEscape={true}
                        textToHighlight={props.time}/></p>
                    </div>
                </div>
            </div>
        )
    }
}

function SearchContactInfo(props){
    return(
        <div>
        <p className="card-text">City: {props.contact_info.address_city}</p>
        <p className="card-text">County: {props.contact_info.address_county}</p>
        <p className="card-text">State: {props.contact_info.address_state}</p>
        </div>
        );
}

function ContactInfo(props){
    return(
        <div>
        <p className="card-text">City: {props.contact_info.address_city}</p>
        <p className="card-text">County: {props.contact_info.address_county}</p>
        <p className="card-text">State: {props.contact_info.address_state}</p>
        </div>
        );
}

const filtering = {
    'Host District':'place__district',
    'place__district':'place__district',
    'Day':'day',
    'day':'day',
    'Sort by Name':'Sort by Name',
    'Sort by Date':'Sort by Date',
    'Host Institution':'place_name',
    'place_name':'place_name'
     }
 
export default Events;
