import React from "react";
import { Link, Router } from "react-router-dom";

class RelatedEvents extends React.Component{

  constructor(props) {
    super(props)
    this.state={ 'data' : []}

    this.place_id = props.place_id;
  }

  componentDidMount() {
        var targetUrl = 'https://api.booksandbones.me/events/',
        query = "?place="+this.place_id
    fetch(targetUrl + query+"&limit=6")
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
  }

render(){
  return(
      <div>
        <h3>Other events at this location</h3>
         <div className="row">
         <div className="card-group">
        {this.state.data.map(function(item, index){
          return ( 
          <EventCard
            name={item.name}
            website={item.permalink}
            description={item.description.substr(0, 100)+"..."}
            id={item.id}
            image={item.event_image_url}
           />)
        })
      }
      </div>
      </div>
      </div>
      )
    }
}


function EventCard(props){
  return(
      <div className="col-lg-3 col-sm-6 portfolio-item">
            <div className="card h-100">
                <img className="card-img-top" src={props.image} alt=""/>
                <div className="card-body">
                <h4 className="card-title">
                    <Link onClick={() => window.location.refresh()} to={'/event/'+props.id}>{props.name}</Link>
                </h4>''
                    <div className="card-body">
                        <p className="card-text">{props.description}</p>
                        <p className="card-text"><a href={props.website}>Website</a></p>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default RelatedEvents;
