import React from "react";
import { Link } from "react-router-dom";
import Twitter from "./Twitter.js"
import HashTag from "./HashTag.js"
import MapWithAMarker from "./MapWithAMarker.js"
import RelatedEvents from "./RelatedEvents.js"

class EventInstance extends React.Component{
	constructor(props) {
		super(props)
		this.state = { }
	}

	componentDidMount() {
    var targetUrl = 'https://api.booksandbones.me/events/'
		fetch(targetUrl + this.props.match.params.id)
			.then(response => response.json())
			.then(response => this.setState(response))
			.catch(err => console.error(this.props.url, err.toString()));
	}

	render(){
		if (this.state.id){
		return(
      <div>
        <EventPage state={this.state}/>
      </div>)
    } return ( <div></div>);
  }
}

class EventPage extends React.Component{
    constructor(props) {
    super(props)
    this.state=props.state
  }

  componentDidMount() {
      var targetUrl = 'https://api.booksandbones.me/libraries/'
    fetch(targetUrl + this.state.place+"?format=json")
      .then(response => response.json())
      .then(response => this.setState({"host": response}))
      .catch(err => console.error(this.props.url, err.toString()));
  }

  render(){
    if (this.state.host){
    return(
      <div class="container">
        <h1 class="mt-4 mb-3"><Link to={'/events/'}>Events</Link> / {this.state.name}</h1>
          <div class="row">
              <div class="col-md-8">
                <img class="img-fluid" src={this.state.event_image_url} alt=""></img>
              </div>

              <div class="col-md-4">
                <h3 class="my-3">Contact</h3>
                <ul>
          <li>Phone: {this.state.host.contact_info[0].phone}</li>
          <li>Host District: {this.state.host.district}</li>
          <li>Website: <a href={this.state.permalink}> Here </a></li>
          <li>Address: {this.state.host.contact_info[0].address_address}, {this.state.host.contact_info[0].address_city}, {this.state.host.contact_info[0].address_state}, {this.state.host.contact_info[0].address_zip}</li>
        </ul>

                <h3 class="my-3">Event Time</h3>
                <ul>
                    <li>{this.state.pub_date}</li>
                </ul>
              </div>

          </div>
          <br></br>
          <HashTag twitter={"Austin"}/>
          
          <row>
            <h3 class="my-3">Event Description</h3>
            <p>{this.state.description}</p>
          </row>  
      
            <div class="row">
              <Twitter twitter={"Austin"}/>
              <PlaceCard 
                image={this.state.host.image_url}
                contact_info={this.state.host.contact_info}
                name={this.state.host.name}
                place_id={this.state.host.id}/>
              <div class="col-md-4">
                <MapWithAMarker 
                  lat={parseFloat(this.state.host.latitude)} 
                  lng={parseFloat(this.state.host.longitude)} 
                /></div>
            </div> 
            <RelatedEvents place_id={this.state.host.id}/>
        </div>
        );
  } return(<div></div>)}
  
}

function PlaceCard(props){
    return (
        <div class="col-md-4">
            <div className="card h-100">
            	<div class="card-header"> Host Institution </div>
                <img className="card-img-top" src={props.image} alt=""/>
                <div className="card-body">
                    <Link to={'/library/'+props.place_id}><h4 className="card-title">{props.name}</h4></Link>
                    <ContactInfo contact_info={props.contact_info[0]}/>
                </div>
            </div>
        </div>
    );
}

//<ContactInfo contact_info={props.contact_info[0]}/>

function ContactInfo(props){
    return(
        <div>
        <p className="card-text"><a href={props.contact_info.website}>Website</a></p>
        </div>
        );
}

export default EventInstance;
