import { backendAPI }  from '../config.js'
import axios from 'axios';

export async function getEventPage(offset) {
    const response = await axios.get(`${backendAPI}events/?format=json&limit=12&offset=${offset}`);
    return response.data;
}
