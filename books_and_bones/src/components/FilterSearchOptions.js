import React from "react";
class FilterSearchOptions extends React.Component{
    constructor(props){
        super(props)
        this.state = {}
    }
    componentDidMount(){
        for (var i=0;i<this.props.tuples.length;i++){
            this.populateState(this.props.tuples[i][1])
        }
    }
    populateState(fieldName){
        var targetUrl = 'https://api.booksandbones.me/event-distinct?field='+fieldName+'&format=json'
        fetch(targetUrl)
            .then(response => {return response.json()})
            .then(response => this.setState({[fieldName]:response}))
    }
    render(){
        return (
            <form>
                <div class="form-row">
                {this.props.tuples.map(
                    tuple => (
                        <Dropdown
                            title={tuple[0]}
                            fieldName={tuple[1]}
                            key={tuple[0]}
                            options={(this.state[tuple[1]])?this.state[tuple[1]]:[]}             
                        />
                    ))
                }
                <input type="text" id="searchbar" className="form-control mr-sm-2" placeholder="Search" name="search" />
                <input type="submit" value="Submit" />
                </div>
            </form>
        )
    }
}
function Dropdown(props){
    return(
        <div class="form-group" style={{paddingRight:20}}>
            <label>{props.title}</label>
            <select className="form-control" name={props.fieldName}  >
                <option value="">--------------</option>
                {props.options.map(
                    i => (
                        <option placeholder={props.title} value={i}>{i}</option>
                    )
                )}
            </select>
        </div>                
    );
}
export default FilterSearchOptions;