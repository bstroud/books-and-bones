import React from 'react';
import {TwitterHashtagButton} from 'react-twitter-embed';

class HashTag extends React.Component {

	constructor(props) {
		super(props);
		this.twitter = props.twitter;
	}

	render() {
		return (
		  <div>
		  <TwitterHashtagButton
		    tag={this.twitter}
		  />
		  </div>
		)
	}

}

export default HashTag;
