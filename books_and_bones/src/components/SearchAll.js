//modified from https://scotch.io/tutorials/build-custom-pagination-with-react
//modified from https://react-native-training.github.io/react-native-elements/docs/searchbar.html
import './Models.css'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Pagination from "./Pagination.js";
import { ButtonToolbar, DropdownButton, Dropdown } from "react-bootstrap";
import React, { Component, Fragment } from 'react';
import App from './App'
import ReactDOM from "react-dom";
import Highlighter from "react-highlight-words";


const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

var searchQuery = "";
var display = "libraries";

var totalInstances = 0;
/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class SearchAll extends React.Component {

  constructor(props) {
        super(props)

        var urlParams = new URLSearchParams(window.location.search);
        searchQuery = urlParams.get('search');

        this.state = { instancesPerPage: 9,
         data: [], 
         currentPage: null, 
         totalPages: null }

        this.displayLibraries = this.displayLibraries.bind(this);
        this.displayMuseums = this.displayMuseums.bind(this);
        this.displayEvents = this.displayEvents.bind(this);
        this.cards = this.cards.bind(this);
    }

    onPageChanged = data => {
        const { currentPage, totalPages, pageLimit } = data;

        var offset=(currentPage-1)*pageLimit
        var targetUrl = 'https://api.booksandbones.me/' + display + '/?format=json'

        fetch(targetUrl+"&offset="+offset+"&search="+searchQuery+"&limit="+pageLimit)
            .then(response => response.json())
            .then(response => {
                const data=response['results']
                this.setState({ currentPage, data, totalPages });
                this.setState({'total':response['count']})
            })
    
        };

  componentDidMount() {
        //gets total to pass to pagination
        var targetUrl = 'https://api.booksandbones.me/libraries/?format=json'
        fetch(targetUrl)
            .then(response => response.json())
            .then(response => this.setState({'total':response['count']}))
            .catch(error => console.error(error));

    }

    displayLibraries() {
        display = "libraries"
        var targetUrl = 'https://api.booksandbones.me/libraries/?format=json'
        fetch(targetUrl+"&search="+searchQuery)
            .then(response => response.json())
            .then(response => {
                const data=response['results']
                this.setState({ data })
                this.setState({'total':response['count']})
            })
    }

    displayMuseums() {
        display = "museums"
        var targetUrl = 'https://api.booksandbones.me/museums/?format=json'
        fetch(targetUrl+"&search="+searchQuery)
            .then(response => response.json())
            .then(response => {
                const data=response['results']
                this.setState({data})
                this.setState({'total':response['count']})
            })
    }

    displayEvents() {
        display = "events"
        var targetUrl = 'https://api.booksandbones.me/events/?format=json'
        fetch(targetUrl+"&search="+searchQuery)
            .then(response => response.json())
            .then(response => {
                const data=response['results']
                this.setState({data})
                this.setState({'total':response['count']})
            })
    }

    cards(props) {
        if (display === "libraries") {
            return (
                <div className="row">
                <div className="card-group">
                {this.state.data.map(function(item, index){
                return ( 
                    <LibraryPlaceCard
                        name={item.name}
                        id={item.id}
                        type={item.mus_type}
                        description={item.description}
                        district={item.district}
                        contact_info={item.contact_info}
                    />)
                    })
                }
                </div>
                </div>
            )
        } else if (display === "museums") {
            return (
                <div className="card-group">
                {this.state.data.map(function(item, index){
                return ( 
                    <MuseumPlaceCard
                        name={item.name}
                        id={item.id}
                        type={item.mus_type}
                        description={item.summary}
                        contact_info={item.contact_info}
                        image={item.image_url}
                    />)
                    })
                }
                </div>
            )
        } else {
            return (
                <div className="card-group">
                    {this.state.data.map(function(item, index){
                    return ( 
                        <EventCard
                            name={item.name}
                            website={item.permalink}
                            description={item.description}
                            id={item.id}
                            image={item.event_image_url}
                            contact_info={item.contact_info}
                            time={item.pub_date}
                        />)
                        })
                    }
                </div>
            )
        }
    }

    render() {
        var searched = "Search " + display + ": " + searchQuery;
        if(this.state.total){
        return   (
            <div>
            <div className="container">
             <h1 className="my-4">Search Books and Bones</h1>
            <img src="../images/search-bones.jpg" height="74" width="100" alt="bones pic"/>
             <p>{searched}</p>
            </div>
            <div className="container">
                <div className="row">
                <button onClick={this.displayLibraries} className="model-button" type="button">Libraries</button>
                <p>&emsp;</p>
                <button onClick={this.displayMuseums} className="model-button" type="button">Museums</button>
                <p>&emsp;</p>
                <button onClick={this.displayEvents} className="model-button" type="button">Events</button>
                <br></br>
                <br></br>
                </div>
                </div>
                <div className="container">
                <this.cards/>
                <br></br>
                </div>
                <div className="container">
                <div className="row">
                    <Pagination 
                        totalRecords={this.state.total} 
                        pageLimit={this.state.instancesPerPage} 
                        pageNeighbours={1} 
                        onPageChanged={this.onPageChanged}/>
                </div>
                </div>
                <br></br>
                </div>
             );
            } else {
                return (
                <div>
                <div className="container">
                 <h1 className="my-4">Search Books and Bones</h1>
                 <img src="../images/search-books.jpg" height="74" width="100" alt="bones pic"/>
                 <p>{searched}</p>
                 <p>No results found</p>
                 <div className="row">
                 <button onClick={this.displayLibraries} className="model-button" type="button">Libraries</button>
                <p>&emsp;</p>
                <button onClick={this.displayMuseums} className="model-button" type="button">Museums</button>
                <p>&emsp;</p>
                <button onClick={this.displayEvents} className="model-button" type="button">Events</button>
                </div>
                <br></br>
                </div>
                </div>
                )
            }
        }
    }

function LibraryPlaceCard(props){
    if (props.description.toLowerCase().includes(searchQuery.toLowerCase())) {
            var index = props.description.toLowerCase().indexOf(searchQuery.toLowerCase());
            var len = searchQuery.length
            if(index < 50){
                var beg = ""
                var new_index = 0;
            } else {
                var beg = "..."
                var new_index = index - 50;
            }
            var description = beg + props.description.substr(new_index, len + 100) + "...";
        } else {
            var description = props.description.substr(0, 100)+"...";
        }

            return (
                <div className="col-lg-10 col-sm-10">
                    <div className="card h-100">
                        <div className="card-body">
                            <Link to={'/library/'+props.id}><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={searchQuery.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.name}/></Link>

                            <p className="card-text"><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={searchQuery.split(" ")}
                            autoEscape={true}
                            textToHighlight={description}/></p>

                            <p className="card-text">District: <Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={searchQuery.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.district}/></p>

                            <LibraryContactInfo contact_info={props.contact_info[0]}/>
                        </div>
                    </div>
                </div>
    );
}

function LibraryContactInfo(props){
    return(
        <div>
        <p className="card-text">City: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={searchQuery.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_city}/></p>

        <p className="card-text">Zipcode: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={searchQuery.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_zip}/></p>
        </div>
        );
}

function MuseumPlaceCard(props){
        if (props.description.toLowerCase().includes(searchQuery.toLowerCase())) {
            var index = props.description.toLowerCase().indexOf(searchQuery.toLowerCase());
            var len = searchQuery.length
            if(index < 50){
                var beg = ""
                var new_index = 0;
            } else {
                var beg = "..."
                var new_index = index - 50;
            }
            var description = beg + props.description.substr(new_index, len + 100) + "...";
        } else {
            var description = props.description.substr(0, 100)+"...";
        }

            return (
                <div className="col-lg-10 col-sm-10">
                    <div className="card h-100">
                        <div className="card-body">
                            <Link to={'/museum/'+props.id}><p className="card-title"><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={searchQuery.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.name}/></p></Link>

                            <p className="card-text"><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={searchQuery.split(" ")}
                            autoEscape={true}
                            textToHighlight={description}/></p>

                            <p className="card-text">Category: <Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={searchQuery.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.type}/></p>

                            <MuseumContactInfo contact_info={props.contact_info[0]}/>
                        </div>
                    </div>
                </div>
            )
}

function MuseumContactInfo(props){

    return(
        <div>
        <p className="card-text">City: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={searchQuery.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_city}/></p>
        <p className="card-text">County: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={searchQuery.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_county}/></p>
        </div>
        );
}

function EventCard(props){
    if (props.description.toLowerCase().includes(searchQuery.toLowerCase())) {
            var index = props.description.toLowerCase().indexOf(searchQuery.toLowerCase());
            var len = searchQuery.length
            if(index < 50){
                var beg = ""
                var new_index = 0;
            } else {
                var beg = "..."
                var new_index = index - 50;
            }
            var description = beg + props.description.substr(new_index, len + 100);
        } else {
            var description = props.description.substr(0, 100)+"...";
        }
        return (
            <div className="col-lg-10 col-sm-10">
                <div className="card h-100">
                    <div className="card-body">
                        <Link to={'/event/'+props.id}><Highlighter
                        highlightClassName="YourHighlightClass"
                        searchWords={searchQuery.split(" ")}
                        autoEscape={true}
                        textToHighlight={props.name}/></Link>

                        <p className="card-text"><Highlighter
                        highlightClassName="YourHighlightClass"
                        searchWords={searchQuery.split(" ")}
                        autoEscape={true}
                        textToHighlight={description}/></p>

                        <p className="card-text"><Highlighter
                        highlightClassName="YourHighlightClass"
                        searchWords={searchQuery.split(" ")}
                        autoEscape={true}
                        textToHighlight={props.time}/></p>
                    </div>
                </div>
            </div>
    )
}

export default SearchAll;
