import React from "react";
import {withScriptjs,withGoogleMap,GoogleMap,Marker} from "react-google-maps";
import { compose, withProps } from "recompose"

class MapWithAMarker extends React.Component {

  constructor(props) {
    super(props)
    this.lat = props.lat;
    this.lng = props.lng;
  }

  render() {
    return (
      <div>
      <MapAndMarker lat={this.lat} lng={this.lng}/>
      </div>
    )
  }

}

const MapAndMarker = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyD8GNBun-s7TSJrDwdob-A6ny-Qg4XRil8&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
   <GoogleMap
    defaultZoom={15}
    defaultCenter={{ lat: props.lat, lng: props.lng}}
  >
  <Marker
      key={props.id}
      position={{ lat: props.lat, lng: props.lng }}
  />
  </GoogleMap>
);

export default MapWithAMarker;
