import { backendAPI }  from '../config.js'
import axios from 'axios';

export async function getMuseumPage(offset) {
    const response = await axios.get(`${backendAPI}museums/?format=json&limit=12&offset=${offset}`);
    return response.data;
}
  
