import axios from 'axios';

const url = "http://api.booksandbones.me/" 

export async function getMuseumPage(offset) {
    const response = await axios.get(`${url}museums/?format=json&limit=12&offset=${offset}`);
    return response.data;
}

export async function getEventPage(offset) {
    const response = await axios.get(`${url}events/?format=json&limit=12&offset=${offset}`);
    return response.data;
}

export async function getLibraryPage(offset) {
    const response = await axios.get(`${url}libraries/?format=json&limit=12&offset=${offset}`);
    return response.data;
}

export async function getLibraryData(id) {
    const response = await axios.get(url + `libraries/${id}/?format=json`);
        return response.data;
}

export async function getMuseumData(id) {
    const response = await axios.get(url + `museums/${id}/?format=json`);
        return response.data;
}

export async function getEventData(id) {
    const response = await axios.get(url + `events/${id}/?format=json`);
        return response.data;
}
