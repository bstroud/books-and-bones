import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Twitter from "./Twitter.js"
import HashTag from "./HashTag.js"
import MapWithAMarker from "./MapWithAMarker.js"

class MuseumInstance extends React.Component{
	constructor(props) {
		super(props)
		this.state = {}
	}

	componentDidMount() {
    var targetUrl = 'https://api.booksandbones.me/museums/'
		fetch(targetUrl + this.props.match.params.id)
			.then(response => response.json())
			.then(response => this.setState(response))
			.catch(err => console.error(this.props.url, err.toString()));
	}

	render(){
    if (this.state.id){
      if (this.state.image_url === ""){
        this.setState({"image_url":"https://brusselsmuseums.imgix.net/covers/Collections-permanentes-du-Musee-dIxelles-Claude-Marchal-Brussels-2017.jpg"})
      }
		  return(
	       <div className="container">
        <h1 class="mt-4 mb-3"><Link to={'/museums/'}>Museums</Link> / {this.state.name}</h1>

    <div class="row">

      <div class="col-md-8">
        <img class="img-fluid" src={this.state.image_url} alt=""></img>
      </div>

      <div class="col-md-4">
      {this.state.contact_info.map(function(item, index){
            return ( 
            <div>
            	<h3 class="my-3">Contact</h3>
        <ul>
          <li>Location: {item.address_city}, {item.address_county} County, {item.address_state}</li>
        </ul></div>)
            	})
            }
      </div>

    </div>
    <br></br>
    <HashTag twitter={"TexasMuseums"}/>
    <row>
      <h3 class="my-3">Museum Description</h3>
      <p>{this.state.summary}</p>

      <div class="row">
      <div class="col-md-4">
          <Twitter twitter={"TexasMuseums"}/>
      </div>
      <div class="col-md-4">
      
      <MapWithAMarker lat={parseFloat(30.269565)} lng={parseFloat(-97.724044)} />
      </div>
      </div>

    </row>
    <div class="row">
    </div>
    <Other address_city={this.state.contact_info[0].address_city} />
    </div>
        );
      }
    return ( <div className="container">
    <h1 className="my-4">{this.state.name}</h1></div> )
	}
}

class Other extends React.Component{
    constructor(props) {
    super(props)
    this.state={ 'data' : []}
  }

  componentDidMount() {
      var targetUrl = 'https://api.booksandbones.me/museums/',
        query = "?city="+this.props.address_city
    fetch(targetUrl + query +"&limit=6")
      .then(response => response.json())
      .then(response => this.setState({'data':response['results']}))
      .catch(err => console.error(this.props.url, err.toString()));
  }

  render(){
    return(
        <div>
          <h3>Other Museums in this City</h3>
           <div className="row">
           <div className="card-group">
          {this.state.data.map(function(item, index){
            return (
            <MuseumCard
              name={item.name}
              id={item.id}
              website={item.permalink}
              description={item.summary}
              image_url={item.image_url}
             />)
          })
        }
        </div>
        </div>
        </div>
        )
    }
}

function MuseumCard(props){
  return(
      <div className="col-lg-3 col-sm-6 portfolio-item">
            <div className="card h-100">
                <img className="card-img-top" src={props.image_url} alt=""/>
                <div className="card-body">
                <h4 className="card-title">
                    <Link onClick={() => window.location.refresh()}  to={'/museum/'+props.id}>{props.name}</Link>
                </h4>
                    <div className="card-body">
                        <p className="card-text">{props.description}</p>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default MuseumInstance;

