from api.search import searchEvents, searchMuseums, searchLibraries


def filterEvents(queryset, request):
    city = request.query_params.get("city")
    place = request.query_params.get("place_name")
    day = request.query_params.get("day")
    library = request.query_params.get("place")
    district = request.query_params.get("place__district", None)
    sort = request.query_params.get("sort", None)
    if library is not None:
        queryset = queryset.filter(place=library)
    if sort is not None:
        queryset = queryset.order_by(sort)
    if district is not None:
        queryset = queryset.filter(place__district__icontains=district)
    if day is not None:
        queryset = queryset.filter(pub_date__icontains=day)
    if city is not None:
        queryset = queryset.filter(place__contact_info__address_city__icontains=city)
    if place is not None:
        queryset = queryset.filter(place__name__icontains=place)
    search = request.query_params.get("search")
    if search is not None:
        queryset = searchEvents(queryset, search)
    beg = request.query_params.get("start_after")
    end = request.query_params.get("start_before")
    if beg is not None and end is not None:
        queryset = queryset.filter(date__level__lte=end)
        queryset = queryset.filter(date__level__gte=beg)
    return queryset


def filterMuseums(queryset, params):
    mus_type = params.get("mus_type", None)
    city = params.get("city", None)
    county = params.get("county", None)
    search = params.get("search", None)
    sort = params.get("sort", None)

    if mus_type is not None:
        queryset = queryset.filter(mus_type__icontains=mus_type)
    if city is not None:
        queryset = queryset.filter(contact_info__address_city__icontains=city)
    if sort is not None:
        queryset = queryset.order_by(sort)
    if search is not None:
        queryset = searchMuseums(queryset, search)
    if county is not None:
        queryset = queryset.filter(contact_info__address_county__icontains=county)
    return queryset


def filterLibraries(queryset, params):
    if params.get("zip") is not None:
        queryset = queryset.filter(
            contact_info__address_zip__icontains=params.get("zip")
        )
    if params.get("district") is not None:
        queryset = queryset.filter(district=params.get("district"))
    if params.get("sort") is not None:
        queryset = queryset.order_by(params.get("sort"))
    if params.get("search") is not None:
        queryset = searchLibraries(queryset, params.get("search"))
    return queryset
