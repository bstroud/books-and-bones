from django.db import models
from django.utils import timezone


class Library(models.Model):
    name = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=200, blank=True, default="")
    district = models.CharField(max_length=200, blank=True, default="")
    open_hours = models.CharField(max_length=200, blank=True, default="")
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default=0.0)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default=0.0)
    image_url = models.CharField(max_length=100, blank=True, default="")

    def __str__(self):
        return self.name


class Museum(models.Model):
    name = models.CharField(max_length=200, unique=True)
    summary = models.CharField(max_length=200, blank=True, default="")
    mus_type = models.CharField(max_length=200, blank=True, default="")
    image_url = models.CharField(max_length=100, blank=True, default="")

    def __str__(self):
        return self.name


class Contact_info(models.Model):
    place = models.ForeignKey(
        Library,
        related_name="contact_info",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    place_museum = models.ForeignKey(
        Museum,
        related_name="contact_info",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    phone = models.CharField(max_length=20, blank=True, default="")
    website = models.CharField(max_length=100, blank=True, default="")
    address_address = models.CharField(max_length=200, blank=True, default="")
    address_city = models.CharField(max_length=200, blank=True, default="")
    address_state = models.CharField(max_length=200, blank=True, default="")
    address_zip = models.CharField(max_length=200, blank=True, default="")
    address_county = models.CharField(max_length=200, blank=True, default="")


class Event(models.Model):
    place = models.ForeignKey(Library, related_name="events", on_delete=models.CASCADE)
    place_name = models.CharField(max_length=200, default="")
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    permalink = models.CharField(max_length=100, unique=True)
    pub_date = models.CharField(max_length=200)
    event_image_url = models.CharField(max_length=100)
    date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name


# class City(models.Model):
#     name = models.CharField(max_length = 100)
#     state = models.CharField(max_length = 100)
#     county = models.CharField(max_length = 100)
