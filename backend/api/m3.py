import requests
from bs4 import BeautifulSoup
from api.models import Museum, Contact_info


def createMusuemObjects(m_dict):

    museum = Museum(
        name=m_dict["name"],
        summary=m_dict["summary"],
        mus_type=m_dict["musType"],
        image_url=m_dict["image_url"],
    )
    museum.save()
    contact = Contact_info(
        place_museum=museum,
        address_city=m_dict["city"],
        address_county=m_dict["county"],
        address_state="TX",
    )
    contact.save()


def go():
    response = requests.get(
        "https://en.wikipedia.org/wiki/List_of_museums_in_Central_Texas"
    )
    soup = BeautifulSoup(response.text, "html.parser")
    table = soup.find("table", class_="wikitable")

    rows = table.find_all("tr")

    header_row = rows[1]

    for item in rows[2:]:
        data = item.find_all("td")
        name = data[0].text
        if data[1].find("img"):
            image_url = "http:" + (data[1].find("img")["src"]).replace(
                "125px", "1000px"
            )
        else:
            image_url = ""
        city = data[2].text
        county = data[3].text
        musType = data[4].text
        summary = data[5].text
        m_dict = {
            "name": name,
            "city": city,
            "county": county,
            "image_url": image_url,
            "musType": musType,
            "summary": summary,
        }
        createMusuemObjects(m_dict)
        # print(obj)
