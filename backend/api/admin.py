from django.contrib import admin
from .models import Library, Event, Museum, Contact_info

admin.site.register(Library)
admin.site.register(Event)
# admin.site.register(Museum)
# Register your models here.


class Contact_InfoInline(admin.StackedInline):
    model = Contact_info
    extra = 0


class MuseumAdmin(admin.ModelAdmin):
    fields = ["name"]
    inlines = [Contact_InfoInline]


admin.site.register(Museum, MuseumAdmin)
