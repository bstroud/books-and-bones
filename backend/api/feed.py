import xml.etree.ElementTree as ET
import requests
from api.models import Library, Contact_info, Event
from bs4 import BeautifulSoup
import datetime


def image_scraper(site):

    response = requests.get(site)

    soup = BeautifulSoup(response.text, "html.parser")
    img_tags = soup.find_all("img")

    urls = [img["src"] for img in img_tags]

    for url in urls:
        if "http" in url:
            return url


def loadRSS():

    # url of rss feed
    url = "https://library.austintexas.gov/events-feed.xml"

    # creating HTTP response object from given url
    resp = requests.get(url)
    print(type(resp.text))
    line = resp.text.replace(u"\03", "")
    print(type(line))
    # print(line)
    # saving the xml file
    text_file = open("eventsfeed.xml", "w")
    text_file.write(line)
    text_file.close()


def parseXML(xmlfile):

    # create element tree object
    tree = ET.parse(xmlfile)

    # get root element
    root = tree.getroot()

    # create empty list for news items
    newsitems = []

    # iterate news items
    for item in root.findall("./channel/item"):

        # empty news dictionary
        news = {}

        # iterate child elements of item
        for child in item:

            # special checking for namespace object content:media
            if child.tag == "{http://search.yahoo.com/mrss/}content":
                news["media"] = child.attrib["url"]
            else:
                news[child.tag] = child.text.encode("utf8")

        # append news dictionary to news items list
        newsitems.append(news)

    # return news items list
    return newsitems


def buildEvents():
    # load rss from web to update existing xml file
    loadRSS()
    # # parse xml file
    items = parseXML("eventsfeed.xml")
    for event in items:
        tmp = event["description"].decode("utf-8")
        lib_name_end = tmp.find("-")
        lib = tmp[:lib_name_end].strip()
        description = tmp[lib_name_end + 1 :].strip()
        name = event["title"].decode("utf-8")
        permalink = event["link"].decode("utf-8")
        # print("-----------")
        # print("Name:",name)
        # print("Hosted at:",lib)
        # print("Description:",description)
        # print("Link:",permalink)
        # print("Pub date:",pub_date)
        print(lib)
        placeObj = Library.objects.filter(name=lib)
        pub_date = event["pubDate"].decode("utf-8").strip(" ")[0:-6]
        date_time_obj = datetime.datetime.strptime(pub_date, "%a, %d %b %Y %H:%M:%S")
        if date_time_obj > datetime.datetime.now():
            if Library.objects.filter(name=lib):
                image_url = image_scraper(permalink)
                place = placeObj[0]
                event = Event(
                    place=place,
                    place_name=lib,
                    name=name,
                    description=description,
                    permalink=permalink,
                    event_image_url=image_url,
                    pub_date=pub_date,
                    date=date_time_obj,
                )
                event.save()
                # print(placeObj[0])
                # print("There is at least one Entry with the headline", lib)
            else:
                print("none")
        else:
            print("none")


def test():
    loadRSS()
    # # parse xml file
    items = parseXML("eventsfeed.xml")
    count = 0
    for event in items:
        tmp = event["description"].decode("utf-8")
        lib_name_end = tmp.find("-")
        lib = tmp[:lib_name_end].strip()
        description = tmp[lib_name_end + 1 :].strip()
        name = event["title"].decode("utf-8")
        permalink = event["link"].decode("utf-8")
        pub_date = event["pubDate"].decode("utf-8").strip(" ")[0:-6]
        date_time_obj = datetime.datetime.strptime(pub_date, "%a, %d %b %Y %H:%M:%S")
        if date_time_obj > datetime.datetime.now():
            count += 1
            print(date_time_obj)

    print(count)
