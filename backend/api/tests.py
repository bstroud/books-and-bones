from django.test import TestCase
from api.models import Library, Event, Contact_info, Museum
from django.db import IntegrityError
from api.filter import filterEvents, filterLibraries, filterMuseums


class LibraryTestCase(TestCase):
    def setUp(self):
        Library.objects.create(name="lion")
        Library.objects.create(name="cat")

    def test_libraries_string(self):
        """Animals that can speak are correctly identified"""
        lion = Library.objects.get(name="lion")
        cat = Library.objects.get(name="cat")
        self.assertEqual(str(lion), "lion")
        self.assertEqual(str(cat), "cat")

    def test_libraries_defaults(self):
        library = Library(name="name")
        self.assertEqual(library.description, "")
        self.assertEqual(library.district, "")
        self.assertEqual(library.open_hours, "")
        self.assertEqual(library.latitude, 0.0)
        self.assertEqual(library.longitude, 0.0)
        self.assertEqual(library.image_url, "")

    def test_libraries_unique(self):
        try:
            Library.objects.create(name="lion")
            self.fail("Name should be a unique field")
        except IntegrityError:
            pass


class EventTestCase(TestCase):
    def setUp(self):
        self.library = Library(name="lion")
        self.library.save()

    def test_event_creation1(self):
        try:
            Event.objects.create(name="lion")
            self.fail("Should be required to have a library object")
        except IntegrityError as e:
            pass

    def test_event_creation2(self):
        Event.objects.create(place=self.library, name="name")
        pass

    def test_event_defaults(self):
        event = Event(place=self.library, name="213")
        event.save()
        self.assertEqual(event.name, "213")
        self.assertEqual(event.place_name, "")
        self.assertEqual(event.description, "")


class MuseumTestCase(TestCase):
    def setUp(self):
        self.museum = Museum(
            name="m", summary="summary", mus_type="aviation", image_url="google.com"
        )
        self.museum.save()

    # Test that name is unique
    def test_museum_unique(self):
        try:
            Museum.objects.create(name="m")
            self.fail("Duplicate museums should not be allowed")
        except IntegrityError as e:
            pass

    # Test that all fields that are not name are unique
    def test_museum_creation(self):
        m3 = Museum(
            name="m3", summary="summary", mus_type="aviation", image_url="google.com"
        )
        m3.save()
        pass

    def test_museum_defaults(self):
        m2 = Museum(name="m2")
        m2.save()
        self.assertEqual(m2.name, "m2")
        self.assertEqual(m2.summary, "")
        self.assertEqual(m2.mus_type, "")
        self.assertEqual(m2.image_url, "")


class ContactTestCase(TestCase):
    def setUp(self):
        self.lib = Library(name="lib")
        self.mus = Museum(name="mus")
        self.lib.save()
        self.mus.save()

    def test_contact_creation1(self):
        contact = Contact_info(website="google.com")
        contact.save()
        self.assertEqual(contact.website, "google.com")

    def test_contact_creation2(self):
        contact = Contact_info(place=self.lib, website="google.com")
        contact.save()
        self.assertEqual(contact.website, "google.com")

    def test_contact_creation3(self):
        contact = Contact_info(place_museum=self.mus, website="google.com")
        contact.save()
        self.assertEqual(contact.website, "google.com")


class FilterLibrariesTestCase(TestCase):
    from rest_framework import request

    def setUp(self):
        Library.objects.create(name="a", district=1)
        Library.objects.create(name="b", district=2)
        Library.objects.create(name="c", district=3)
        Library.objects.create(name="d", district=3)
        Contact_info.objects.create(
            place=Library.objects.get(name="a"), address_zip="777"
        )
        Contact_info.objects.create(
            place=Library.objects.get(name="b"), address_city="san"
        )
        Contact_info.objects.create(
            place=Library.objects.get(name="c"), address_zip="333"
        )
        self.queryset = Museum.objects.all()
        self.queryset = Library.objects.all()

    def test_FilterLibraries_1(self):
        params = {"zip": "78723"}
        result = filterLibraries(self.queryset, params)
        self.assertFalse(result)

    def test_FilterLibraries_2(self):
        params = {"district": 3}
        result = filterLibraries(self.queryset, params)
        self.assertTrue(result.count() == 2)

    def test_FilterLibraries_3(self):
        params = {"district": 1}
        result = filterLibraries(self.queryset, params)
        self.assertTrue(result.count() == 1)

    def test_FilterLibraries_4(self):
        params = {"zip": "777"}
        result = filterLibraries(self.queryset, params)
        self.assertEqual(result.count(), 1)

    def test_FilterLibraries_5(self):
        params = {"district": 1, "zip": "777"}
        result = filterLibraries(self.queryset, params)
        self.assertTrue(result.count() == 1)

    def test_FilterLibraries_6(self):
        params = {"district": 2, "zip": "777"}
        result = filterLibraries(self.queryset, params)
        self.assertEqual(result.count(), 0)

    def test_FilterLibraries_7(self):
        params = {"district": 3, "zip": "333"}
        result = filterLibraries(self.queryset, params)
        self.assertEqual(result.count(), 1)


class FilterMuseumsTestCase(TestCase):
    from rest_framework import request

    def setUp(self):
        Museum.objects.create(name="a", mus_type="1")
        Museum.objects.create(name="b", mus_type="2")
        Museum.objects.create(name="c", mus_type="3")
        Museum.objects.create(name="d", mus_type="3")

        Contact_info.objects.create(
            place_museum=Museum.objects.get(name="a"),
            address_city="Austin",
            address_county="Travis",
        )
        Contact_info.objects.create(
            place_museum=Museum.objects.get(name="b"), address_city="san"
        )
        Contact_info.objects.create(
            place_museum=Museum.objects.get(name="c"), address_city="san"
        )
        self.queryset = Museum.objects.all()

    def test_FilterMuseums_1(self):
        params = {"mus_type": "0"}
        result = filterMuseums(self.queryset, params)
        self.assertFalse(result)

    def test_FilterMuseums_2(self):
        params = {"mus_type": "3"}
        result = filterMuseums(self.queryset, params)
        self.assertTrue(result.count() == 2)

    def test_FilterMuseums_3(self):
        params = {"mus_type": "1"}
        result = filterMuseums(self.queryset, params)
        self.assertTrue(result.count() == 1)

    def test_FilterMuseums_4(self):
        params = {"county": "Travis"}
        result = filterMuseums(self.queryset, params)
        self.assertTrue(result.count() == 1)

    def test_FilterMuseums_5(self):
        params = {"county": "Brazos"}
        result = filterMuseums(self.queryset, params)
        self.assertTrue(result.count() == 0)

    def test_FilterMuseums_6(self):
        params = {"city": "Austin"}
        result = filterMuseums(self.queryset, params)
        self.assertTrue(result.count() == 1)

    def test_FilterMuseums_7(self):
        params = {"city": "Dallas"}
        result = filterMuseums(self.queryset, params)
        self.assertTrue(result.count() == 0)

    def test_FilterMuseums_8(self):
        params = {"city": "san"}
        result = filterMuseums(self.queryset, params)
        self.assertTrue(result.count() == 2)

    def test_FilterMuseums_9(self):
        params = {"city": "Austin", "county": "Travis"}
        result = filterMuseums(self.queryset, params)
        self.assertEqual(result.count(), 1)

    def test_FilterMuseums_10(self):
        params = {"city": "Austin", "county": "Travis", "mus_type": "1"}
        result = filterMuseums(self.queryset, params)
        self.assertEqual(result.count(), 1)

    def test_FilterMuseums_11(self):
        params = {"city": "Austin", "county": "Travis", "mus_type": "3"}
        result = filterMuseums(self.queryset, params)
        self.assertEqual(result.count(), 0)
